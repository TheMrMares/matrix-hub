import { Matrix, RawMatrix } from './matrix'
import { displayMatricesDimensions } from './helpers'

const mergePairOfMatrices = (matrixA, matrixB) => {
  if (matrixA.columns() !==  matrixB.columns() || matrixA.rows() !== matrixB.rows()) {
    throw new Error(`You cannot merge 2 matrices of different sizes. ${displayMatricesDimensions([matrixA, matrixB])}`)
  }
  let tmpMatrix = new Matrix(null, matrixA.columns(), matrixA.rows())
  for(let i = 0; i < matrixA.columns(); i++) {
    for(let j = 0; j < matrixA.rows(); j++) {
      tmpMatrix.set(i, j, matrixA.get(i, j) + matrixB.get(i, j))
    }
  }
  return tmpMatrix
}

const subtractPairOfMatrices = (matrixA, matrixB) => {
  if (matrixA.columns() !==  matrixB.columns() || matrixA.rows() !== matrixB.rows()) {
    throw new Error(`You cannot subtract 2 matrices of different sizes. ${displayMatricesDimensions([matrixA, matrixB])}`)
  }
  let tmpMatrix = new Matrix(null, matrixA.columns(), matrixA.rows())
    for(let i = 0; i < matrixA.columns(); i++) {
      for(let j = 0; j < matrixA.rows(); j++) {
        tmpMatrix.set(i, j, matrixA.get(i, j) - matrixB.get(i, j))
      }
    }
  return tmpMatrix
}

const multiplyPairOfMatrices = (matrixA, matrixB) => {
  if (matrixA.columns() !==  matrixB.rows()) {
    throw new Error(`You cannot multiply matrices if number of rows is diffrent than number of columns. ${displayMatricesDimensions([matrixA, matrixB])}`)
  }
  let tmpMatrix = new Matrix(null, matrixB.columns(), matrixA.rows())
  for(let i = 0; i < matrixA.columns(); i++) {
    for(let j = 0; j < matrixA.rows(); j++) {
      if (typeof matrixB === 'object') {
        let finalCellValue = 0;
        for(let k = 0; k < matrixA.columns(); k++) {
          finalCellValue +=  matrixA.get(k, j) * matrixB.get(i, k)
        }
        tmpMatrix.set(i, j, finalCellValue)
      } else if (typeof matrixB === 'number') {
        tmpMatrix.set(i, j, matrixA.get(i, j) * matrixB)
      }
    }
  }
  return tmpMatrix
}

const mergeMatrices = matrices => {
  const mergedMatrices = matrices.reduce((accumulaator, currentValue) => {
    return mergePairOfMatrices(accumulaator, currentValue)
  })
  return mergedMatrices
}

const subtractMatrices = matrices => {
  const subtractedMatrices = matrices.reduce((accumulaator, currentValue) => {
    return subtractPairOfMatrices(accumulaator, currentValue)
  })
  return subtractedMatrices
}

const multiplyMatrices = matrices => {
  const multipliedMatrices = matrices.reduce((accumulaator, currentValue) => {
    return multiplyPairOfMatrices(accumulaator, currentValue)
  })
  return multipliedMatrices
}

const Mx = {
  Matrix,
  RawMatrix,

  mergePairOfMatrices,
  subtractPairOfMatrices,
  multiplyPairOfMatrices,
  mergeMatrices,
  subtractMatrices,
  multiplyMatrices,
}

export default Mx
