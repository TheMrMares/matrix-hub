export const displayMatricesDimensions = matrices => {
  const arrayOfDimensions = matrices.map(matrix => `[${matrix.columns()} columns / ${matrix.rows()} rows]`)
  return arrayOfDimensions.join(' <> ')
}
