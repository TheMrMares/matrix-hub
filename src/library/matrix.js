
export class RawMatrix {
  constructor(matrix) {
    this.__data = matrix
    this.__columns = matrix.length
    this.__rows = this.findLengthOfLongestRow()
    this.fillUndefined(0)
  }

  data() {
    return this.__data
  }

  rows() {
    return this.__rows
  }

  columns() {
    return this.__columns
  }

  findLengthOfLongestRow() {
    let initLength = 0
    for(let i = 0; i < this.__data.length; i++) {
      if (this.__data[i].length > initLength) {
        initLength = this.__data[i].length
      }
    }
    return initLength
  }

  fillUndefined(value) {
    const columns = this.findLengthOfLongestRow()
    for(let i = 0; i < this.__data.length; i++) {
      if (this.__data[i].length < columns) {
        for(let j = 0; j < columns - this.__data[i].length; j++) {
          this.__data[i].push(value || 0)
        }
      }
    }
    return this
  }
}

export class Matrix extends RawMatrix {

  constructor(matrix, x, y, fillValue) {
    if (typeof matrix === 'object' && matrix !== null) {
      super(matrix)
    } else {
      const tmpArray = []
      for(let i = 0; i < y; i ++) {
        tmpArray.push([])
        for(let j = 0; j < x; j ++) {
          tmpArray[i].push(fillValue)
        }
      }
      super(tmpArray)
    }
  }

  get(column, row) {
    if(row) {
      if ( column > this.columns - 1 || row > this.__rows - 1) {
        throw new Error('Row or column index is bigger than number of row/columns')
      }
      if(column < 0 || row < 0) {
        throw new Error('Column and row cannot be 0 or less.')
      }
    }
    if (column !== undefined && row !== undefined) {
      return this.__data[row][column]
    }
    if (row !== undefined && column === undefined) {
      return this.__data[row]
    }
    if (row === undefined && column !== undefined) {
      return this.__data[row].map(r => r[column])
    }
    return this.__data
  }

  set(column, row, value) {
    if(row) {
      if ( column > this.columns - 1 || row > this.__rows - 1) {
        throw new Error('Row or column index is bigger than number of row/columns')
      }
      if(column < 0 || row < 0) {
        throw new Error('Column and row cannot be 0 or less.')
      }
    }
    if (column !== undefined && row !== undefined) {
      this.__data[row][column] = value
    }
    return this
  }

  transpose() {
    const { __rows, __columns } = this
    for(let i = 0; i < __columns; i++) {
      for(let j = 0; j < __rows; j++) {
        this.set(j, i, this.get(i, j))
      }
    }
    this.__columns = __rows;
    this.__rows = __columns;

    return this
  }


}

export default Matrix
