import Mx from './library/core'

const myMatrix = new Mx.Matrix([
  [1, 5, 3],
  [2, 4, 9],
  [-4, 10, 12]
])
const myMatrix2 = new Mx.Matrix([
  [5, 2, 7],
  [6, 14, 11],
  [-14, 3, -6]
])
const myMatrix3 = new Mx.Matrix(null, 3, 3, 10)

console.log('myMatrix', myMatrix)
console.log('myMatrix2', myMatrix2)
console.log('myMatrix3', myMatrix3)

const mergedMatrix = Mx.mergePairOfMatrices(myMatrix, myMatrix2)
console.log('merged myMatrix & myMatrix2', mergedMatrix)

const subtractedMatrix = Mx.subtractPairOfMatrices(myMatrix, myMatrix2)
console.log('subtracted myMatrix & myMatrix2', subtractedMatrix)

const multipliedMatrix = Mx.multiplyPairOfMatrices(myMatrix, myMatrix2)
console.log('multiplied myMatrix & myMatrix2', multipliedMatrix)

const mergedMatrices = Mx.mergeMatrices([myMatrix, myMatrix2, myMatrix3, myMatrix])
console.log('merged myMatrix & myMatrix2 & myMatrix3  & myMatrix', mergedMatrices)

const subtractedMatrices = Mx.subtractMatrices([myMatrix, myMatrix2, myMatrix3, myMatrix])
console.log('subtracted myMatrix & myMatrix2 & myMatrix3  & myMatrix', subtractedMatrices)

const multipliedMatrices = Mx.multiplyMatrices([myMatrix, myMatrix2, myMatrix3, myMatrix])
console.log('multiplied myMatrix & myMatrix2 & myMatrix3  & myMatrix', multipliedMatrices)

myMatrix.transpose();
console.log('inverted myMatrix', myMatrix);
